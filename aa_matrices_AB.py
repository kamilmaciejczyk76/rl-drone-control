from math import *
import numpy as np

def  aa_matrices_AB( RHS , x , t , u , n , m ):
  deli = 1.0e-6
  f0 =  RHS ( x , t , u )
  A = np.zeros((n,n))
  B = np.zeros((n,m))

  for j in range( 0 , n ):
    dx = np.zeros( (n , ) );
    dx[j] = deli;
    A[:,j]=(  RHS ( x + dx , t , u ) - f0 ) / deli;

  
  for j in range (0 , m):
    du = np.zeros( (m ,) );
    du[j] = deli;
    B[:,j]=(  RHS ( x , t , u + du ) - f0 ) / deli;

  return A, B