# Optimal  flight control of UAV

## Summary

The purpose of this study is to analyze the possibility of using the LQR algorithm to control the optimal flight of the drone avoiding an obstacle. The paper also presents the history of drones, mathematical modeling of flying objects and their numerical implementation. In order to conduct experiments, differential equations were derived that represent the mathematical model of the drone, and then a numerical program in Python was created, implementing the control for a given mathematical model along a given trajectory. The trajectory implemented in the program reflects the actual elevation of the terrain that the drone should avoid. Numerical simulations were carried out to fine-tune the LQR algorithm, and then to check the quality of control at different drone flight speeds and different weights of the drone. It has been proved that the mass does not have a significant effect on the operation of the algorithm and that the flight speed should be carefully controlled, as it interferes with the control. The results showed that the LQR algorithm is an appropriate tool to control drones so that it is possible to effectively avoid the obstacle.

Author: Kamil Maciejczyk, kamilmaciejczyk76@gmail.com  
Language of study: Polish



