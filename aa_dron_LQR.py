import matplotlib.pyplot as plt
import numpy as np
from math import *


from aa_euler import aa_euler
from aa_matrices_AB import aa_matrices_AB
from aa_rhs import aa_rhs
from aa_trajectory import aa_trajectory
from lqr_m import lqr_m

from aa_rhs import mass

n = 6;  # dimension of x
#n = 8;  # With dynamic engines
m = 2;  # dimension of u

z0 = 2.0;
h_flight = 1.0;  # Over the terrain
# c_turb = 1000.0;
# X_turb_1 = 1500.0;
# X_turb_2 = 2000.0;

c_turb = 0.0;
X_turb_1 = 0.0;
X_turb_2 = 0.0;

az_turbulence = 0.0;
ax_wind = 0.0;
az_wind = 0.0;

rad2deg = 180 / pi;
deg2rad = pi / 180;

if n == 6:
    x = np.array([0.0, 0.0, 0.0, 0.0, -z0, 0.0])

if n == 8:
    x = np.array([0.0, 0.0, 0.0, 0.0, -z0, 0.0, 0.0, 0.0])

u = np.array([0.0, 0.0])

Vel = 0.1;  # / 3.6;

dt = 0.01;
t = 0.0;

tp = np.zeros(20000)
yp = np.zeros((len(x), 20000))
up = np.zeros((len(u), 20000))
gp = np.zeros(20000)
zp = np.zeros(20000)
ez_avg_list = []

plt.figure("Wynik", figsize=(15, 6))

q = 10
r = 1

R = np.eye(m, m);
Q = np.eye(n, n);
Q[0, 0] = 1000.0;
Q[1, 1] = 1000.0;
Q[2, 2] = 0.1;
Q[3, 3] = 10.0;
Q[4, 4] = 100.0;
Q[5, 5] = 1.0e+03;

if (n == 8):
  q = 8000
  r = 0.3
  Q = q * Q
  R = r * R
  
if (n == 6):
  q = 10
  r = 1
  Q = q * Q
  R = r * R
    
e = np.zeros((n,))
        
for i in range(0, 20000):
    X = x[3]
    if i % 500 == 0: print(str(floor(X*100/4)) + '%')
    if X > 3:
        break

    

    Z0 = 5.0;
    [z_terr, alfa] = aa_trajectory(X, Vel, dt);
    Vx = Vel * cos(alfa);
    Vz = Vel * sin(alfa);

    z_ref = z_terr + h_flight;

    tp[i] = t;
    yp[:, i] = x
    up[:, i] = u;
    gp[i] = z_terr;
    zp[i] = z_ref;

    x_ref = X;

    

    
    e[0] = x[0] - (cos(x[5]) * Vx + sin(x[5]) * Vz);
    e[1] = x[1] - (sin(x[5]) * Vx - cos(x[5]) * Vz);
    e[2] = x[2] - 0;
    # e[3] = x[3] - (x_ref);
    e[3] = 0.0;
    e[4] = x[4] - (-z_ref);
    e[5] = x[5] - 0.0;
    
    ez_avg_list.append(e[4])

    [A, B] = aa_matrices_AB(aa_rhs, x, t, u, n, m);
    [K, P] = lqr_m(A, B, Q, R);
    u = np.squeeze(np.asarray(np.dot(-K, e)));

    umax = 100000.0;
    u[0] = max(-umax, min(umax, u[0]));
    u[1] = max(-umax, min(umax, u[1]));

    az_turbulence = 0.0;
    ax_wind = 0.0;
    az_wind = 0.0;
    if X > X_turb_1 and X < X_turb_2:
        az_turbulence = c_turb * (1.0 - 2.0 * rand());
        ax_wind = 0.0;
        az_wind = 0.0;  # 15.5 + 3.0*( 1.0 - 2.0*rand() );

    # x = ( "aa_rhs" , x , t , dt , u );
    x = aa_euler(aa_rhs, x, t, dt, u);
  

    if i % 500 == 0:
        # refresh;

        xl = 0;  # max( 0 , X - 1 );
        xp = 5.0;
        zl = 0.0;
        zu = 8.0;

        v_x = x[0]
        v_z = x[1]
        V = sqrt(x[0] ** 2 + x[1] ** 2);
        e_v = Vel - V;
        teta = x[5] * rad2deg;
        alt = -x[4];
        T1 = u[0];
        T2 = u[1];
        gamma = atan((sin(x[5]) * x[0] - cos(x[5]) * x[1]) / (cos(x[5]) * x[0] + sin(x[5]) * x[1])) * rad2deg;

        # figure( 'position' , [ 300 , 100 , 1100 , 640 ] );
        # txt = "t={7.3f} V={10.5f} m/s  v_x=#10.5f v_z={10.5f}  teta={10.5f}  alfa=#10.5f |||  u1={7.4f}  u2={7.4f}   |||   e_v={10.5f}  e(z)={10.5}".format(t , V , v_x , v_z , teta , alfa*rad2deg , T1 , T2 , e_v , e(5) )
        txt = "t={:.3f}  V={:.5f} m/s  v_x={:.5f}  v_z={:.5f}  teta={:.5f}  alfa={:.5f} |||".format(t, V, v_x, v_z,
                                                                                                    teta,
                                                                                                    alfa * rad2deg)
        txt += "u1={:.4f}  u2={:.4f}   |||".format(T1, T2)
        txt += "e_v={:.5f}  e(z)={:.5}".format(e_v, e[5])

        # axis( [ xl, xp, zl , zu ] );

        plt.title(txt)
        plt.plot(yp[3, :][:i], zp[:][:i], 'r')
        plt.plot(yp[3, :][:i], gp[:][:i], 'g')
        plt.plot(yp[3, :][:i], -yp[4, :][:i], 'b')
        plt.plot(yp[3, i], -yp[4, i], 'bo')

        plt.show(block=False)
        plt.pause(0.05)
        plt.clf()

        # plt.axis(  [ 300 , 100 , 1100 , 640 ] )

    t = t + dt;

xl = 0;  # max( 0 , X - 1 );
xp = 5.0;
zl = 0.0;
zu = 8.0;

v_x = x[0]
v_z = x[1]
V = sqrt(x[0] ** 2 + x[1] ** 2);
e_v = Vel - V;
teta = x[5] * rad2deg;
alt = -x[4];
T1 = u[0];
T2 = u[1];
gamma = atan((sin(x[5]) * x[0] - cos(x[5]) * x[1]) / (cos(x[5]) * x[0] + sin(x[5]) * x[1])) * rad2deg;

# figure( 'position' , [ 300 , 100 , 1100 , 640 ] );
# txt = "t={7.3f} V={10.5f} m/s  v_x=#10.5f v_z={10.5f}  teta={10.5f}  alfa=#10.5f |||  u1={7.4f}  u2={7.4f}   |||   e_v={10.5f}  e(z)={10.5}".format(t , V , v_x , v_z , teta , alfa*rad2deg , T1 , T2 , e_v , e(5) )
txt = "t={:.3f}  V={:.5f} m/s  v_x={:.5f}  v_z={:.5f}  teta={:.5f}  alfa={:.5f} |||".format(t, V, v_x, v_z,
                                                                                            teta,
                                                                                            alfa * rad2deg)
txt += "u1={:.4f}  u2={:.4f}   |||".format(T1, T2)
txt += "e_v={:.5f}  e(z)={:.5}".format(e_v, e[5])

# axis( [ xl, xp, zl , zu ] );

#plt.title(txt)
plt.plot(yp[3, :][:i], zp[:][:i], 'r')
plt.plot(yp[3, :][:i], gp[:][:i], 'g')
plt.plot(yp[3, :][:i], -yp[4, :][:i], 'b')
plt.plot(yp[3, i], -yp[4, i], 'bo')

ez_avg = sum(abs(number) for number in ez_avg_list) / len(ez_avg_list)

path = 'wyniki\\n=' + str(n) + 'm=' + str(mass) + 'q=' + str(q) + 'r=' + str(r) + 'ez_avg=' + str(ez_avg) + 'vx=' + str(Vel) + '.png'
print(path)


plt.savefig(path, dpi=300)
plt.show()
# with open(path + '.csv', "a") as f:
#   f.write('Q \n')
#   np.savetxt(f, Q, delimiter=";")
#   f.write('R \n')
#   np.savetxt(f, R, delimiter=";")
#   f.write('n={} \n'.format(n))
#   f.write('m={} \n'.format(mass))
