from math import *
import numpy as np
from rotor import calculateThrust


#TO DO: import from main script
az_turbulence = 0.0
ax_wind = 0.0
az_wind = 0.0
mass = 25

def aa_rhs( x , t , u ):

  n = np.size(x, 0)
  dx_dt = np.zeros( n )

  deg2rad = pi/180.0
  g = 9.81

  S = 1.0
  
  
  Iy = 100
  #parametry smigla i silnika
  Is = 0.05 #moment bezwl zespolu wirnika i smigla
  d = 0.1 # srednica [m]
  A = pi*d**2 / 4

  U = 12 #V
  # zakladamy ze u = I, czyli sterujemy natężeniem prądu

  ######################################################

  vx = x[0] + ax_wind
  vz = x[1] + az_wind

  alpha = atan2( vz , vx )
  V = sqrt( vz*vz + vx*vx )

  CD_0 = 0.30
  CD = CD_0

  ro_0 = 1.225
  ro = ro_0 * ( 1.0 - ( abs( x[4] ) / 44300.0 ) )**4.256

  Q_dyn = 0.5*ro*V*V

  L = 0.0
  D = Q_dyn * S * CD
  G = mass * g

  Th = 1.0
  
  Thrust_1 = 0.5*G + u[0]
  Thrust_2 = 0.5*G + u[1]
 
  if( n == 8 ):
    #modyfikacja predkosci katowej
    om_min = 120 #minimalna wartosc prędkosci katowej
    if x[6] < om_min: x[6] = om_min
    if x[7] < om_min: x[7] = om_min
    
    # momenty obrotowe silników elektrycznych Me = Pe/w = U*I/w
    #M1 = U * u[0] / x[6]
    #M2 = U * u[1] / x[7]
    # x[6] i x[7] to predkosci katowe silnikow drona
    coef = 5000
    Thrust_1 = calculateThrust(x[6]) * coef
    Thrust_2 = calculateThrust(x[7]) * coef
    # coef = 0.001
    # Thrust_1 = coef * x[6]
    # Thrust_1 = coef * x[7]
  


  cm_q = -0.01
  
  Tau = 0.05

  beta = 0.0*deg2rad
  cb = cos(beta)
  sb = sin(beta)
  
  #TODO opisac ladnie równania
  # uklad zwiazamy z obiektem
  # dvx/dt = ax
  dx_dt[0] = ( -D*cos(alpha) + L*sin(alpha) - G*sin(x[5]) - Thrust_1*sb + Thrust_2*sb ) / mass  -  x[2]*vz
  # dvz/dt = az
  dx_dt[1] = ( -D*sin(alpha) - L*cos(alpha) + G*cos(x[5]) - Thrust_1*cb - Thrust_2*cb ) / mass  +  x[2]*vx  + az_turbulence
  # dw/dt = epsilon
  dx_dt[2] = ( 0.5*( Thrust_2*cb - Thrust_1*cb ) + cm_q*x[2] ) / Iy

  #uklad nieruchomy
  # vX
  dx_dt[3] =  cos(x[5])*vx + sin(x[5])*vz
  # vZ
  dx_dt[4] = -sin(x[5])*vx + cos(x[5])*vz
  # Omega
  dx_dt[5] = x[2]

  # rownania wynikajaca z sily elektromotorycznej silnika ---- I * dw/dt = M -----
  # predkosci katowe smigiel
  if( n == 8 ):
    #dw1/dt 
    dx_dt[6] =  ( U * u[0] / x[6] ) / Is
    #dw2/dt
    dx_dt[7] =  ( U * u[1] / x[7] ) / Is
    
  
  return dx_dt


