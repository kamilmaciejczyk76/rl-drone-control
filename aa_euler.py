from math import *
import numpy as np

def aa_euler( RHS , x , t , dt, u ):
  c_bet = 0.9;
  del_state = 1.0e-6;
  coef = - c_bet / del_state;
  n = x.shape[0]
  y = np.zeros( (n , ) );
  Jacob = np.zeros( (n , n) );
  y0 = RHS( x , t , u )
  for i in range(0, n):
    tmp = x[i];
    x[i] = x[i] + del_state;
    vec = RHS ( x , t , u )
    x[i] = tmp;
    Jacob[:,i] = coef * ( vec - y0 );

  Jacob = Jacob + np.eye(n,n) / dt;
  dx = np.dot(np.linalg.inv(Jacob) , y0)
  y = x + dx;

  return y