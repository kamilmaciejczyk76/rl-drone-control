from math import *
import numpy as np

def lqr_m(a,b,q,r,nn=0):

  # %LQR	Linear quadratic regulator design for continuous-time systems.
  # %	[K,S] = LQR(A,B,Q,R)  calculates the optimal feedback gain matrix K
  # %	such that the feedback law  u = -Kx  minimizes the cost function:
  # %
  # %		J = Integral {x'Qx + u'Ru} dt
  # %
  # %	subject to the constraint equation: 
  # %		.
  # %		x = Ax + Bu 
  # %                
  # %	Also returned is S, the steady-state solution to the associated 
  # %	algebraic Riccati equation:
  # %				  -1
  # %		0 = SA + A'S - SBR  B'S + Q
  # %
  # %	[K,S] = LQR(A,B,Q,R,N) includes the cross-term N that relates
  # %	u to x in the cost function.
  
  # %	J.N. Little 4-21-85
  # %	Revised 8-27-86 JNL
  # %	Copyright (c) 1985, 1986 by the MathWorks, Inc.

  # %error(nargchk(4,5,nargin));
  # %error(abcdchk(a,b));
  
  m = np.ma.size(a, 0)
  n = np.ma.size(a, 1)
  nb = np.ma.size(b, 0)
  nb = np.ma.size(b, 1)
  mq = np.ma.size(q, 0)
  nq = np.ma.size(q, 1)
  
  if (m != mq) or (n != nq): 
  	error('A and Q must be the same size')
  
  mr = np.size(r, 0)
  nr = np.size(r, 1)
  
  if (mr != nr) or (nb != mr):
  	print('B and R must be consistent')
  
  #if nargin == 5:
  if False:
    mn = size(nn, 0)
    nnn = size(nn, 1)
    
    if (mn != m) or (nnn != nr):
      print('N must be consistent with Q and R')
  	
    # Add cross term
    q = q - nn / r * np.transpose(nn)
    a = a - b / r * nop.transpose(nn)
  else:
    nn = np.zeros((m, nb))
  
  # Start eigenvector decomposition by finding eigenvectors of Hamiltonian:
  matrix = np.bmat( [ [a, np.dot(np.dot(b, np.linalg.inv(r)), np.transpose(b))], [q, -np.transpose(a)] ] )
  [d,v] = np.linalg.eig(matrix)
  d = np.real(d);
  index = np.argsort(np.real(d))
  d = np.sort(np.real(d));	 # sort on real part of eigenvalues
  
   
  # #if (~( (d(n)<0) && (d(n+1)>0) ))
  # if not ( (d[n-1]<1.e-15) and (d[n]>-1.e-15) ):
  #     print('Can''t order eigenvalues, (A,B) may be uncontrollable. Checking rank C = [B A*B ... A^(n-1)*B ]\n')
  #     C = np.zeros((m,n*nb))
  #     c = b;
  #     C[:,1:nb] = c;
  #     for i in range(0, n-1):
  #       c = a*c;
  #       C[:,i*nb+1:i*nb+nb] = c;
  #     #C
  #     rank_C = rank(C);
  #     if( rank_C < n ):
  #       rank_C
  #       print('rank_C < n , (A,B) are uncontrollable.')
  #     else:
  #       print('rank_C = n (OK), but there is something wrong with ordering - check it out!')
  
  chi = v[0:n,index[0:n]];	 # select vectors with negative eigenvalues
  lambd = v[(n):(2*n),index[0:n]]
  s = -np.real(np.linalg.solve(chi.conj().T, lambd.conj().T).conj().T)
  #s = -np.real(lambd/chi)
  s[np.isnan(s)] = 0
  #k = r \ (np.transpose(nn) + np.matmul(np.transpose(b) , s))
  k = np.linalg.solve(r, np.transpose(nn) + np.matmul(np.transpose(b) , s))
  
  return k, s